package databaseconnection;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


/*
This class make the connections to the database.
 */
public class DbConnectionManager
{
    private static final String URL = "jdbc:mysql://localhost:3308/musictable_test";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "zerg";
    private final Connection connection;

    // This function connects the database from MYSQL.
    public DbConnectionManager()
    {
        Connection connection1;

        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection1 = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        }
        catch(ClassNotFoundException ex)
        {
            System.out.println("Error: unable to load driver class!");
            ex.printStackTrace();
            connection1 = null;
        }
        catch(java.sql.SQLException e)
        {
            System.out.println("Cannot make a connection to the database.");
            connection1 = null;
        }

        connection = connection1;
    }

    // The getter for the connection
    public Connection getConnection()
    {
        return connection;
    }

    // The SQL input for teh connection.
    public ResultSet sqlInput(String statement) throws SQLException
    {
        ResultSet rs = connection.createStatement().executeQuery(statement);
        return rs;
    }
}
