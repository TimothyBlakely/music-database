module com.example.databaseproject {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires java.sql;
    requires mysql.connector.java;


    opens com.example.databaseproject to javafx.fxml;
    exports com.example.databaseproject;
}