package com.example.databaseproject;


import java.sql.PreparedStatement;
import java.sql.ResultSetMetaData;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.ResultSet;

import static com.example.databaseproject.IOManager.dbcManager;

public class PrintOutChosenArtist extends MenuOption
{
    private static final String SQL_INSERT_STATEMENT = "SELECT songtitle, album, songlength, realeaseyear FROM songs" +
            " WHERE songartist = (SELECT id FROM artist WHERE bandname = ?);";

    @Override
    public void run()
    {
        try
        {
            System.out.println("What artist do you want to display the info for? ");
            String choice = IOManager.input.next();
            PreparedStatement ps = dbcManager.getConnection().prepareStatement(SQL_INSERT_STATEMENT);
            ps.setString(1, choice);

            try
            {
                ResultSet rs = ps.executeQuery();
                ResultSetMetaData rsmd = rs.getMetaData();
                int columnsNumber = rsmd.getColumnCount();
                System.out.println("These are all the songs by " + choice);

                for(int i = 1; i <= columnsNumber; i++)
                {
                    System.out.print(rsmd.getColumnName(i) + " ");
                }
                System.out.println();
                while (rs.next())
                {
                    for (int i = 1; i <= columnsNumber; i++)
                    {
                        if (i > 1) System.out.print(",  ");
                        String columnValue = rs.getString(i);
                        System.out.print(columnValue);
                    }
                    System.out.println("");
                }
            }
            catch (SQLIntegrityConstraintViolationException e)
            {
                System.out.println("The artist " + choice + " is not in the table.");
            }
        }
        catch (java.sql.SQLException e)
        {
            System.out.println("SQL error");
            e.printStackTrace();
        }

    }
}
