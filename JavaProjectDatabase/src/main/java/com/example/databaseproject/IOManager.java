package com.example.databaseproject;


import databaseconnection.DbConnectionManager;

import java.util.Scanner;


/*
This is an input class for all inputs from user data.
 */
public class IOManager
{
    public static Scanner input = new Scanner(System.in);
    public static DbConnectionManager dbcManager = new DbConnectionManager();
}
