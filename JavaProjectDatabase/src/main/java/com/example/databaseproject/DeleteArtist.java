package com.example.databaseproject;

import java.sql.PreparedStatement;
import java.sql.SQLIntegrityConstraintViolationException;
import static com.example.databaseproject.IOManager.dbcManager;


/*
This class removes an artist from a table.
 */
public class DeleteArtist extends MenuOption
{
    private static final String SQL_INSERT_STATEMENT = "DELETE FROM artist WHERE bandname = ?;";

    @Override
    public void run()
    {
        try
        {
            System.out.println("What artist do you want to remove? ");
            String choice = IOManager.input.next();
            PreparedStatement ps = dbcManager.getConnection().prepareStatement(SQL_INSERT_STATEMENT);
            ps.setString(1, choice);

            try
            {
                ps.executeUpdate();
                System.out.println("The artist " + choice + " has been removed.");
            }
            catch (SQLIntegrityConstraintViolationException e)
            {
                System.out.println("The artist " + choice + " is not in the table.");
            }
        }
        catch (java.sql.SQLException e)
        {
            System.out.println("SQL error");
        }

    }
}
