package com.example.databaseproject;

import java.sql.ResultSet;

import static com.example.databaseproject.IOManager.dbcManager;


/*
This class displays all artists in the table.
 */
public class DisplayArtist extends MenuOption
{
    @Override
    public void run()
    {
        try
        {
            ResultSet rs = dbcManager.sqlInput("SELECT * FROM artist;");
            while (rs.next()) {
                System.out.println(rs.getString(2));
            }
        }
        catch (java.sql.SQLException e)
        {
            System.out.println("SQL error");
        }

    }
}
