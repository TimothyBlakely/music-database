package com.example.databaseproject;

import static com.example.databaseproject.IOManager.input;


/*
The menu for user input.
 */
public class DatabaseMenu
{
    static MenuOption[] menuOptions = new MenuOption[] {new DisplayArtist(),new AddArtist(), new DeleteArtist(), new PrintOutChosenArtist()};


    public static void main(String[] args)
    {
        while(DatabaseMenu.runMenu())
        {}
    }

    // This is the menus for adding or removing from the database.
    static public boolean runMenu()
    {
        System.out.println("\nSelect a number.\n 1) Print all artists?\n 2) Add a new artist?\n 3) Delete an artist? \n " +
                "4) Print out songs by a chosen artist? \n 5) Exit menu?");
        int menuNum = input.nextInt();

        if(menuNum != 5)
        {
            MenuOption menuOption = menuOptions[menuNum - 1];
            menuOption.run();
        }
        return menuNum != 5;
    }

}
