package com.example.databaseproject;

import databaseconnection.DbConnectionManager;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Scanner;
import static com.example.databaseproject.IOManager.dbcManager;


/*
This class adds an artist to the artist table
 */
public class AddArtist extends MenuOption
{
    private static final String SQL_INSERT_STATEMENT = "INSERT INTO artist (id, bandname) VALUES ('11', ?);";

    // This adds an artist the artist table, and does not accept duplicates.
    @Override
    public void run()
    {
        try
        {

            System.out.println("What is the artist name? ");
            String choice = IOManager.input.next();
            PreparedStatement ps = dbcManager.getConnection().prepareStatement(SQL_INSERT_STATEMENT);
            ps.setString(1, choice);

            try
            {
                ps.executeUpdate();
                System.out.println("The artist " + choice + " is added to the table.");
            }
            catch (SQLIntegrityConstraintViolationException e)
            {
                System.out.println("The artist " + choice + " is already in the artist table.");
            }
        }
        catch (java.sql.SQLException e)
        {
            System.out.println("SQL error");
        }

    }

}
