package com.example.databaseproject;

import databaseconnection.DbConnectionManager;

import java.sql.SQLException;

/*
    The abstract class for all the inputs from other methods.
 */
public abstract class MenuOption
{

    public MenuOption() {}

    public abstract void run();
}
