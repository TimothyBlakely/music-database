




CREATE SCHEMA IF NOT EXISTS musictable_test;
CREATE SCHEMA IF NOT EXISTS musictable_test1 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE musictable_test1;
DROP TABLE IF EXISTS songs; 
DROP TABLE IF EXISTS artist;
CREATE TABLE IF NOT EXISTS artist (
id int NOT NULL,
bandname VARCHAR(20) UNIQUE NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS songs (
id_song int NOT NULL,
songartist int NOT NULL,
songtitle VARCHAR(35)NOT NULL,
album VARCHAR(35)NOT NULL,
songlength int NOT NULL,
realeaseyear int NOT NULL,
PRIMARY KEY (id_song),
  CONSTRAINT fk_songs_songartist
    FOREIGN KEY (songartist)
    REFERENCES artist (id)
    ON DELETE CASCADE 
    ON UPDATE CASCADE
);

/*
Do not create a new artist table, only add to this artist table otherwise it will screw up the ERD diagram.
*/
INSERT INTO artist (id, bandname) VALUES ('1', 'Pantera');
INSERT INTO artist (id, bandname) VALUES ('2', 'Slipknot');
INSERT INTO artist (id, bandname) VALUES ('3', 'Metallica');
INSERT INTO artist (id, bandname) VALUES ('4', 'Black Sabbath');
INSERT INTO artist (id, bandname) VALUES ('5', 'Sepultura');
INSERT INTO artist (id, bandname) VALUES ('6', 'Mudvayne');
INSERT INTO artist (id, bandname) VALUES ('7', 'Megadeath');
INSERT INTO artist (id, bandname) VALUES ('8', 'Slayer');
INSERT INTO artist (id, bandname) VALUES ('9', 'Linkin Park');
INSERT INTO artist (id, bandname) VALUES ('10', 'Arch Enemy');



INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('1', '4','iron man','Paranoid',355,'1970');
INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('2', '3','enter sandman','The Black Ablum',331,'1991');
INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('3', '1','walk','Vulgar Display of Power',316,'1992');
INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('4', '1','5 mintues alone','Far Beyond Driven',360,'1994');
INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('5', '2','wait and bleed','Slipknot',149,'1999');
INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('6', '2','spit it out','Slipknot',179,'1999');
INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('7', '6','dig','ld50',161,'2000');
INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('8', '6','death blooms','ld50',231,'2000');
INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('9', '9','crawling','hybrid theory',219,'2000');
INSERT INTO songs (id_song, songartist, songtitle, album, songlength, realeaseyear) VALUES ('10', '9','one step closer','hybrid theory',157,'2000');

